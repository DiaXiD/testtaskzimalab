<?php
require_once("functions.php");
if (isset($_POST['deleteid'])) {
    $delAcc = new Account();
    $delAcc->setId($_POST['deleteid']);
    $delAcc->deleteAccountFromDb();
}
require_once("header.php");
?>

<div class="container">
    <ul class="list">

        <?php
        if (isset($_GET['page'])) {
            $pagenow = $_GET['page'];
        } else {
            $pagenow = 1;
        }
        $accs = getPageOfAccountsFromDb($pagenow);
        if ($accs == null) {
            exit;
        }
        foreach ($accs as $account) { ?>

            <li class="listelement">
                <div class="column">
                    <h3>
                        <?= $account->getLastName() . " " . $account->getFirstName() ?>
                    </h3>
                    <p>
                        <?php
                        if ($account->getCompany() != '' && $account->getPosition() != '') {
                            echo $account->getCompany() . " | " . $account->getPosition();
                        } else {
                            echo $account->getCompany() . $account->getPosition();
                        } ?>
                    </p>
                </div>
                <div class="column">
                    <p>
                        <?= $account->getEmail() ?>
                    </p>
                </div>
                <div class="column">

                    <?php
                    $phones = $account->getPhones();
                    for ($i = 0; $i <= 2; $i++) {
                        ?>

                        <p>
                            <?= $phones[$i] ?>
                        </p>
                        <?php
                    }
                    ?>
                </div>
                <div class="column controls">
                    <div>
                        <form action="/form.php" method="post">
                            <input hidden type="number" name="accid" value="<?= $account->getId() ?>">
                            <button class="btn-yellow btn" type="submit">EDIT</button>
                        </form>

                    </div>
                    <div>
                        <form action="/" method="post">
                            <input hidden type="number" name="deleteid" value="<?= $account->getId() ?>">
                            <button class="btn-red btn" type="submit">DELETE</button>
                        </form>
                    </div>
                </div>
            </li>
        <?php } ?>

    </ul>
    <div class="pagination">
        <?php
        $pagesCount = getPagesCount();
        for ($pageNum = 1; $pageNum <= $pagesCount; $pageNum++): ?>
            <a href="/?page=<?= $pageNum ?>" class="pagination"><?= $pageNum ?></a>
        <?php endfor; ?>
    </div>
</div>
<?php
require_once("header.php");
?>