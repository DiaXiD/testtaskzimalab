<?php
require_once("functions.php");
require_once('account.php');
require_once("header.php");
?>

<div class="container">
    <?php if (($_GET['message'] == 'notvalid')) {
        echo "Заполните все данныевсе данные";
    }
    ?>
    <form class="form" action="/sendform.php" method="post">
        <?php if (isset($_POST['accid'])) {
            $acc = new Account();
            $acc->getAccountFromDb($_POST['accid']);
            ?>
            <input hidden type="number" name="id" value="<?= $acc->getId() ?>">
        <?php } else {
            $acc = new Account();
        }
        ?>
        <div class="col">
            <label for="firstName">Имя*</label>
            <input required type="text" name="firstName" value="<?= $acc->getfirstName() ?>">
        </div>
        <div class="col">
            <label for="lastName">Фамилия*</label>
            <input required type="text" name="lastName" value="<?= $acc->getLastName() ?>">
        </div>
        <div class="col">
            <label for="company">Компания</label>
            <input type="text" name="company" value="<?= $acc->getCompany() ?>">
        </div>
        <div class="col">
            <label for="position">Должность</label>
            <input type="text" name="position" value="<?= $acc->getPosition() ?>">
        </div>
        <div class="col">
            <label for="email">Email*</label>
            <input required type="email" name="email" value="<?= $acc->getEmail() ?>">
        </div>
        <div class="col">
            <label for="phone">Телефоны</label>
            <input type="text" name="phone" value="<?= $acc->getPhones()[0] ?>">
            <input type="text" name="phone2" value="<?= $acc->getPhones()[1] ?>">
            <input type="text" name="phone3" value="<?= $acc->getPhones()[2] ?>">
        </div>
        <div class="col">
            <button type="submit">Сохранить</button>
        </div>
    </form>
</div>
<?php
require_once("footer.php");
?>