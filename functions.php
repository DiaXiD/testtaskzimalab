<?php
require_once('config.php');
require_once('account.php');
function getPageOfAccountsFromDb(int $page)
{
    global $db, $config;
    $beginOfListAccounts = ($page - 1) * ($config['accountsPerPage']);
    $sql = "SELECT * FROM `accounts_oop` WHERE `accounts_oop`.`deleted` = 0 LIMIT {$config['accountsPerPage']} OFFSET {$beginOfListAccounts};";
    $result = mysqli_query($db, $sql);
    if ($result == false) {
        print("Произошла ошибка при выполнении запроса");
        print(mysqli_error($db));
    }
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    foreach ($rows as $row) {
        $account = new Account();
        $account->setId($row['id']);
        $account->setFirstName($row['first_name']);
        $account->setLastName($row['last_name']);
        $account->setCompany($row['company']);
        $account->setEmail($row['email']);
        $account->setPosition($row['position']);
        $account->setPhones([$row['phone'], $row['phone2'], $row['phone3']]);
        $accounts[] = $account;
    }
    return $accounts;

}
function getPagesCount()
{
    $sql = 'SELECT COUNT(*) FROM `accounts_oop` WHERE `accounts_oop`.`deleted` = false;';
    global $db, $config;
    $result = mysqli_query($db, $sql);
    if ($result == false) {
        print("Произошла ошибка при выполнении запроса");
        print(mysqli_error($db));
    }
    $accountCount = mysqli_fetch_array($result)[0];
    return ceil($accountCount / $config['accountsPerPage']);
}
// Подключение к базе
$db = mysqli_connect("{$config['db']['host']}", "{$config['db']['user']}", "{$config['db']['password']}", "{$config['db']['name']}");
if ($db == false) {
    echo ("Не удалось подключиться к базе!<br>" . mysqli_connect_error());
}

?>