<?php
require_once('account.php');
// Проверка на наличие всех данных, на случай если проверку на фронте обошли
if (($_POST['firstName']=='') || ($_POST['lastName']=='') || ($_POST['email']=='')) {

    header("Location: /form.php?message=notvalid");
    exit();
}

$acc = new Account();
$acc->setFirstName($_POST['firstName']);
$acc->setLastName($_POST['lastName']);
$acc->setCompany($_POST['company']);
$acc->setPhones([$_POST['phone'], $_POST['phone2'], $_POST['phone3']]);
$acc->setPosition($_POST['position']);
$acc->setEmail($_POST['email']);
if (isset($_POST['id'])) {
    $acc->setId($_POST['id']);
    $acc->updateAccountInDb();
} else {
    $acc->addAccountInDb();
}
header("Location: /");
exit();

?>