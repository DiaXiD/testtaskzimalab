<?php


require_once('functions.php');
class Account
{
    private $id;
    private $firstName;
    private $lastName;
    private $company;
    private $position;
    private $email;
    private $phones = ["", "", ""];
    private $deleted = false;
    public function getId()
    {
        return $this->id;
    }
    public function setId(int $id)
    {
        $this->id = $id;
    }
    public function getFirstName()
    {
        return $this->firstName;
    }
    public function setFirstName(string $name)
    {
        $this->firstName = $name;
    }
    public function getLastName()
    {
        return $this->lastName;
    }
    public function setLastName(string $surname)
    {
        $this->lastName = $surname;
    }
    public function getCompany()
    {
        return $this->company;
    }
    public function setCompany(string $company)
    {
        $this->company = $company;
    }
    public function getPosition()
    {
        return $this->position;
    }
    public function setPosition(string $position)
    {
        $this->position = $position;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    public function getPhones()
    {
        return $this->phones;
    }
    public function setPhones(array $phones)
    {

        for ($i = 0; $i < 3; $i++) {
            $this->phones[$i] = $phones[$i];
        }

    }
    public function addAccountInDb()
    {
        global $db;
        $sql = "INSERT INTO `accounts_oop` (`first_name`, `last_name`, `email`, `company`, `position`, `phone`, `phone2`, `phone3`, `deleted`) 
        VALUES ('{$this->firstName}', '{$this->lastName}', '{$this->email}', '{$this->company}', '{$this->position}', '{$this->phones[0]}',
                '{$this->phones[1]}', '{$this->phones[2]}', false);";
        $result = mysqli_query($db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($db));
        } else {
            $this->setId(mysqli_insert_id($db));
        }
    }
    public function updateAccountInDb()
    {
        global $db;
        $sql = "UPDATE `accounts_oop` SET `first_name`='{$this->firstName}', `last_name`='{$this->lastName}', `email`='{$this->email}', 
                        `company`='{$this->company}', `position`='{$this->position}', `phone`='{$this->phones[0]}', `phone2`='{$this->phones[1]}',
                        `phone3`='{$this->phones[2]}' WHERE `accounts_oop`.`id` = {$this->id};";
        $result = mysqli_query($db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($db));
        }
    }
    public function deleteAccountFromDb()
    {
        global $db;
        $sql = "UPDATE `accounts_oop` SET `deleted` = true WHERE `accounts_oop`.`id` = {$this->id};";

        $result = mysqli_query($db, $sql);

        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($db));
        }
    }
    public function getAccountFromDb(int $id)
    {
        $this->setId($id);
        global $db;
        $sql = "SELECT * FROM `accounts_oop` WHERE `accounts_oop`.`id` = {$this->id}";
        $result = mysqli_query($db, $sql);
        $row = mysqli_fetch_array($result);
        $this->setFirstName($row['first_name']);
        $this->setLastName($row['last_name']);
        $this->setEmail($row['email']);
        $this->setCompany($row['company']);
        $this->setPosition($row['position']);
        $this->setPhones([$row['phone'], $row['phone2'], $row['phone3']]);
        $this->deleted = $row['deleted'];
    }

}
?>